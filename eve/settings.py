MONGO_HOST='localhost'
MONGO_PORT=27017
MONGO_DBNAME='movies'
X_DOMAINS = '*'
X_HEADERS = [
    'Authorization',
    'Origin, X-Requested-With',
    'Content-Type',
    'Accept',
    'If-Match'
    ]
RESOURCE_METHODS = ['GET', 'DELETE', 'POST']
ITEM_METHODS = ['GET', 'DELETE', 'PUT', 'PATCH']
VERSIONING = True
SOFT_DELETE = True
URL_PREFIX = 'api'

MONGO_QUERY_BLACKLIST = ['$where']

schema =  {
        'DVD_Title'       : {'type' : 'string' },
        'Studio'          : {'type' : 'string' },
        'Released'        : {'type' : 'string',
                            'nullable' : True
                            },
        'Status'          : {'type' : 'string'},
        'Sound'           : {'type' : 'string'},
        'Versions'        : {'type' : 'string'},
        'Price'           : {'type' : 'number'},
        'Rating'          : {'type' : 'string'},
        'Year'            : {'type' : 'string',
                            'nullable' : True
                            },
        'Genre'           : {'type' : 'string'},
        'Aspect'          : {'type' : 'string'},
        'UPC'             : {'type' : 'integer'},
        'DVD_ReleaseDate' : {'type' : 'string'},
        'ID'              : {'type' : 'integer'},
        'Timestamp'       : {'type' : 'string'},
        }

dvd = {
        # This field just happens to be the same as the first field in the
        # schema and are not related
        'dvd_title': 'dvd',
        'additional_lookup': {
            'url': 'regex("[\w]+")',
            'field': 'DVD_Title'
        },
        'schema': schema,
        'cache_control': 'max-age=0',
        }


DOMAIN = {
    'dvd': dvd,
}
