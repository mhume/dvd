'use strict'

angular.module('dvdApp')
  .controller('MainCtrl', function ($scope, $http) {

    $scope.update = function (string) {
      $http.get('http://finn.opi.arizona.edu:20008/api/dvd?where={%22DVD_Title%22:%20{%22$regex%22:%20%22' + string + '%22}}').success(function (data) {
        $scope.results = data._items
      })
    }

  })
