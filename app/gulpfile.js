var gulp = require('gulp')
var $ = require('gulp-load-plugins')()

gulp.task('connect', function () {
  $.connect.server({
      host: '0.0.0.0',
      port: 2020,
      root: 'app'
  })
})

gulp.task('default', ['connect'])

